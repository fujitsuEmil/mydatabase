CC = gcc

all: main.o database.o
	$(CC) main.o database.o -o database

main.o: main.c database.h
	$(CC) main.c -c -o main.o

data_base.o: database.c database.h
	$(CC) database.c -c -o database.o

clean:
	rm -f *.o database

