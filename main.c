#include <stdio.h>
#include <stdlib.h>
#include "database.h"


int main()
{
    int choice = 0, id_temp = 0, loop = 1;
    Node *list_head = NULL;

    while(loop){
        printf("\n***** Menu *****\n");
        printf(" 1. Add\n 2. Edit\n 3. Save\n 4. Delete\n 5. Display\n 6. Exit\n");
        printf("\nEnter your choice: ");

        choice = 0;
        if(!(scanf("%d", &choice))){
            fprintf(stderr, "Choice should be integer number\n");
            while(getchar() != '\n')
                continue;
        }
        else{
            switch(choice){
            case 1:
                printf("Creating a new node\n");
                add(&list_head);
                break;
            case 2:
                printf("Enter the ID of the contractor you want to edit: ");
                scanf("%d", &id_temp);
                edit(list_head, id_temp);
                break;
            case 3:
                save(list_head);
                printf("Base saved\n");
                break;
            case 4:
                printf("Enter the ID of the contractor you want to delete: ");
                scanf("%d", &id_temp);
                delete(&list_head, id_temp);
                break;
            case 5:
                printf("List of contractors\n");
                display(list_head);
                break;
            case 6:
                exit_list(&list_head);
                list_head = NULL;
                loop = 0;
                break;
            default:
                printf("\nWrong selection!!! Try again!!!");
                break;
            }
            while(getchar() != '\n')
                continue;
        }
    }
    return 0;
}

