#ifndef BASE_H
#define BASE_H

#define SIZE 20
#define FIRST_ID 0;

typedef struct node{
    int id;
    char name[SIZE], surname[SIZE], company[SIZE], position[SIZE];
    int NIP;
    struct node *next;
}Node;

void add(Node **head_node);
void edit(Node *node, int id);
void save(const Node *node);
void delete(Node **head_node, int id);
void display(const Node *node);
void exit_list(Node **node);

#endif
