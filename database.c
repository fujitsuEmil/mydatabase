#include <stdio.h>
#include <stdlib.h>
#include "database.h"


/* Function is adding a node at the end of list */
void add(Node **head_node){
    static int id = FIRST_ID;
    Node *last_node;
    if(head_node == NULL){
        fprintf(stderr, "Pointer to head list is NULL, database is empty\n");
        return;
    }
    Node *new_node = (Node*) malloc(sizeof(Node));
    if(new_node == NULL){
        fprintf(stderr, "Memory not allocated\n");
        exit (1);
    }
    new_node->next = NULL;
    new_node->id = ++id;

    // fujitsu: co poniższa pętla robi, dlaczego tu jest
    while(getchar() != '\n')
        continue;
    printf("Enter your name: ");
    // fujitsu: czy używanie scanf jest bezpieczne?
    scanf("%s", new_node->name);
    printf("Enter your surname: ");
    scanf("%s", new_node->surname);
    printf("Enter company name: ");
    scanf("%s", new_node->company);
    printf("Enter your position: ");
    scanf("%s", new_node->position);
    printf("Enter your NIP number: ");
    scanf("%d", &new_node->NIP);

    if((*head_node) == NULL)
        (*head_node) = new_node;
    else{
        last_node = *head_node;
        // fujitsu: czy mógłbyś przenieść szukanie ostatniego elementu listy do funkcji i ją tu wywołać?
        while(last_node->next != NULL)
            last_node = last_node->next;
        last_node->next = new_node;
    }
}

/* Function edit the node with the given id and changes the structure fields */
void edit(Node *node, int id){
    // fujitsu czy mógłbyś zmodyfikować tą fukcję aby zwracała informację czy znalaziono użytkownika z podanym id?
    if(node == NULL){
        fprintf(stderr, "Database is empty\n");
        return;
    }
    while(node->id != id){
        node = node->next;
        if(node == NULL){
            fprintf(stderr, "There is no such id in the database\n");
            return;
        }
    }
    printf("Enter your name: ");
    scanf("%s", node->name);
    printf("Enter your surname: ");
    scanf("%s", node->surname);
    printf("Enter company name: ");
    scanf("%s", node->company);
    printf("Enter your position: ");
    scanf("%s", node->position);
    printf("Enter your NIP number: ");
    scanf("%d", &node->NIP);
}

/* Function saves contractors' data to a file */
void save(const Node *node){
    // fujitsu zmodyfikuj ta fukcję aby można było podać nazwę pliku do którego chcemy zapisać dane.
    if(node == NULL){
        fprintf(stderr, "Database is empty\n");
        return;
    }
    FILE * pFile;
    pFile = fopen ("myBase.txt","w");
    if(pFile == NULL){
        fprintf(stderr, "Failed to open file.");
        return;
    }
    do {
        fprintf(pFile, "Id: %d\n", node->id);
        fprintf(pFile, "Name: %s\n", node->name);
        fprintf(pFile, "Surname: %s\n", node->surname);
        fprintf(pFile, "Company: %s\n", node->company);
        fprintf(pFile, "Position: %s\n", node->position);
        fprintf(pFile, "NIP number: %d\n", node->NIP);
    }
    while((node = node->next) != NULL);
    fclose (pFile);
}

/* Function searching the id to be deleted */
void delete(Node **head_node, int id){
    if(head_node == NULL ||*head_node == NULL ){
        fprintf(stderr, "Pointer to list head is NULL, database is empty\n");
        return;
    }
    /* removing the first node and changing the head list */
    // fujitsu: dlaczego sprawdzasz czy musimy usunąć pierwszy element
    Node *temp;
    if((*head_node)->id == id){
        temp = *head_node;
        *head_node = (*head_node)->next;
        free(temp);
        return;
    }
    /* removing other nodes */
    Node *current = *head_node;
    Node *previus = NULL;
    while(current->id != id){
        previus = current;
        current = current->next;
        if(current == NULL){
            printf("There is no such id in the database\n");
            return;
        }
    }
    previus->next = current->next;
    free(current);
    printf("Contractor deleted!\n");
}

/* Displays data of contractors starting from the given node */
void display(const Node *node){
    if(node == NULL){
        fprintf(stderr, "Database is empty\n");
        return;
    }
    do {
        printf("Id: %d\n", node->id);
        printf("Name: %s\n", node->name);
        printf("Surname: %s\n", node->surname);
        printf("Company: %s\n", node->company);
        printf("Positin: %s\n", node->position);
        printf("NIP number: %d\n", node->NIP);
    }
    while((node = node->next) != NULL);
}

/* Function releases the allocated memory */
void exit_list(Node **node){
    if(node == NULL){
        fprintf(stderr, "Pointer to list head is NULL, database is empty\n");
        return;
    }
    Node *temp;
    while(*node != NULL){
        temp = (*node)->next;
        free((*node));
        (*node) = temp;
    }
}
